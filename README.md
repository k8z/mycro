```
for doing go-micro development using GitLab. 
 based on the
[helloworld](https://github.com/micro/examples/tree/master/helloworld) Go Micro
template.

- [Go Micro Overview](https://micro.mu/docs/go-micro.html)
- [Go Micro Toolkit](https://micro.mu/docs/go-micro.html)

- main.go - is the main definition of the service, handler and client
- proto - contains the protobuf definition of the API

Dependencies

- [micro](https://github.com/micro/micro)
- [protoc-gen-micro](https://github.com/micro/protoc-gen-micro)

# Run Service

go run main.go

# Query Service

micro call greeter Greeter.Hello '{"name": "OpenSource"}'
```